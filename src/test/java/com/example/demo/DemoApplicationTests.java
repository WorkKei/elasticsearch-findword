package com.example.demo;

import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.testcontainers.elasticsearch.ElasticsearchContainer;

@SpringBootTest
class DemoApplicationTests {

//    @Rule
    ElasticsearchContainer container = new ElasticsearchContainer("docker.elastic.co/elasticsearch/elasticsearch:7.7.1");

//    @Before
//    public void startContainer() {
//        System.out.println("컨테이너 기동!!");
//        container.start();
//    }
//
//    @After
//    public void stopContainer() {
//        System.out.println("컨테이너 종료!!");
//        container.stop();
//    }

    @Test
    void contextLoads() {
        container.start();
        System.out.println(container.getHttpHostAddress());
        System.out.println(container.getHost());
        container.stop();
//        RestHighLevelClient restHighLevelClient = new RestHighLevelClient(RestClient.builder(container.getHost()));
    }

}
